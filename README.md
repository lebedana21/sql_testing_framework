Test harnesses for PostgreSQL codes
------------------------------------

The project contains a test harness for code written in PostgreSQL using [the pgTAP framework](https://pgtap.org/documentation.html). 
It implements a simple, elegant solution allowing isolated parametric testing of code of any complexity.
It allows to run a test case, a test suite or even a complete test project as long as it operates on the same DB. 
 
The repository contains
 
* A Docker image with PostgreSQL 9.6 and pgTAP 1.0.0 (the image is based on [the pgTAP image](https://github.com/walm/docker-pgtap))
* A bash script launching given tests under a database container
* An example of a test project, containing a couple of unit and integration tests

The test project 
-------------

The attached test project implements a couple of tests asserting functionality of a
receipt parser (see [the project specification](test_project/README.md) for details).
 
The project contains two types of modules

* Startup modules (their names start with 'startup')
* Test modules (their names start with 'test')

The purpose of each and detailed description of the types could be found in [the blog post](future_blog_post_text.md).
To make a long story short, first the startup modules will be executed in the database. Those will prepare the database for test execution, will specify startup and shutdown functions etc.
The harness is designed in a way that each test suite (and all its components) is created in a separate database schema. For example, the sample test project represents a single test suite and is created in the *doc_parsing* schema. Importantly, creation of the schema also belongs to a startup module.

The test modules contain test functions. A module might contain a single or multiple test functions. A function, though, specifies a single test case. 

Run the test harness
---------------------

To run the test harnesses on the included example project on your **linux** machine 

* Install Docker if you don't have it yet 
* Clone the repository

```
git clone https://gitlab.com/lebedana21/sql_testing_framework.git
```

* Move to the project root
* Build the [my-pgtap image](my-pgtap-image) 

```
docker build ./my-pgtap-image -t my-pgtap
```

* Run all tests of the test project with [run_tests.sh](run_tests.sh)

```
chmod +x run_test.sh
./run_tests.sh -n sql-testing-cont -p 6432 -t test_project
```

run_tests.sh arguments
------------------------

* `-n` [required] the database container name
* `-p` [required] the database container port
* `-t` [required] path to the tests. Has to be an absolute or a relative path to a folder or to a file. Might be also specified as a pattern, e.g. `tests/test_get_*`.
* `-k` [optional] kill the database container after tests are executed. By default, the container stays alive.
* `-v` [optional] verbose mode (default is non-verbose)


Integration 
-----------

In order to integrate the test harnesses into your project, you will probably modify the database initialization part of [run_tests.sh](run_tests.sh) so that it copies your production database.
You can use a database migration tool of your choice to prepare the test database.


It is recommend to go through [the blog post](future_blog_post_text.md) if you decide to integrate the test harnesses into your project. 
The text contains detailed description of the project structure and requirements. 
The text also clarifies motivation and some technical aspects of the framework.
