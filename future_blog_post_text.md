# Parametric SQL testing with pgTAP - find my way from toy examples to practical application


There is a widely held opinion that having any intelligence written in SQL is a bad, bad idea - it is hard to maintain, release and test.
But in practice SQL is an extremely powerful language which allows to speed up manipulating your data significantly.
Hence developers do write plenty of code in SQL. I also do. And as an agile developer I do 
cover my code with tests as much as I can. 
 
 
In this post I describe an architecture implementing integration parametric testing with the pgTAP package. 
It allows handling test projects of various complexity from one test case to many test suites.
 
Find the project on [gitlab](https://gitlab.com/lebedana21/sql_testing_framework). 

Requirements on The Architecture
-----------------------------

#### Isolation

It is convenient when the test database is isolated and each test accesses the database in its initial shape.
In this way, a developer can avoid many potential bugs, for instance, emerging due to inconsistency of production and test databases.   

#### Parametrization

Toy examples are nice, they are useful. They show user how to access and utilize a solution. 
But in reality things are much more complex. And a test project is not an exception. 


In reality, we can't place the whole test suite in one module (which is a straightforward application of pgTAP package listed in its documentation).
Commonly, a test suite contains many test cases, and a test case itself contains multiple tests. There also are functions all test cases utilize, like *startup* and *shutdown* functions; there is code which prepares the database for a specific test, functions which user is willing to test, etc. 

Hence, we need a structure. We need to introduce parametrization and we need things to be accessible within a test suite. 


#### Simplicity

This is a tricky one. To assure continuous testing a test harness has to be simple and sufficient at the same time. It has to be easy to maintain and to use.
At this point, different kinds of users with various intentions have to be taken in consideration - developers, testers, code reviewers etc. 

First look at pgTAP
------------------

pgTAP is a framework for PostgreSQL TAP-testing. It comes with complete documentation with many toy examples. It also implements a lot of useful assertion functions allowing asserting pretty much everything.
There is a docker image available which is very convenient and it is also worth mentioning that the package allows parallel test runs. 

However, the user has to be aware that the pgTAP is primarily designed for unit testing.
Hence, there is no straightforward way how to utilize the package for a complex test project.

The proposed test harness overcomes these drawbacks. 
Being universal, easy to use and implementing DB isolation, it represents a convenient tool for continuous automated SQL testing.

Implementation details
---------------

### xUnit-style


The tests harness uses xUnit-style for its test functions. 
The style implements *setup* and *teardown* functions, as well as *startup* and *shutdown* - the functionality I found extremely useful. 


The best (but also a bit inconvenient) thing about this approach is that any changes made by a test are rollbacked.
It means that the test database stays clean. But it also means that the developer won't be able to 'take a look' at the data which a test had asserted during an execution, which is kind of convenient in case of integration tests. 
  

### Requirement on structure

#### Test suite schema 

The architecture requires that each test suite is located in its own unique schema.
It allows to separate code of different test suites existing within a test project and also to run a specific suite given a schema name.


#### Test suite content

Often, there is functionality specific for tests within a test suite. 
For instance, there could be a function filling a database table with test data or a function preparing an output table which a test assert will compare against. Proposed tests harness requires declaration of those functions to be in a separate file (one or several) whose name starts with `startup`.  

Any test suite has to contain at least one startup file where **the test suite schema is created** and where a *shutdown* function, removing the schema after tests execution, is defined. The described required content of a *startup* module for `the_schema` schema follows. 
 
```
create schema if not exists the_schema;

create or replace function the_schema.shutdown_drop_the_schema (
) returns VOID as
$func$
begin
  drop schema if exists the_schema cascade;
end
$func$ language plpgsql;
``` 
 

It is required to create test schemas in a *startup* script, since `sql_testing_framework/run_tests.sh` parses all *startup* scripts to collect test schema names.
Then, tests are run sequentially for each found schema. 
 
#### Tests cases

It is up to you whether you put test cases into one or multiple files. I split cases whenever it makes more sense to have the tests separated.
The only requirement is to begin the script name with `test` so that files containing tests are identifiable. 

### Infrastructure

The harness creates a test database in a docker container. At this point, developer has to make sure that the database is in its initial state (e.g. by utilizing a database migration tool). 

As it was already discussed, the approach makes testing much more transparent. But if you don't want to bother yourself with containers and database migration, you can run the test harness against any test database (you will have to slightly modify the `sql_testing_framework/run_tests.sh` script though).
As mentioned above, tests are rollbacked after the execution so the database always stays clean.

Another container with the pgTAP and the pg_prove installed runs test suites of the test project.
As already mentioned, tests will be launched for all suite schemas sequentially. Results are then reported back to the user.
 
 
Example project
------------------

A test project implementing described structure could be found [next to the project source codes](https://gitlab.com/lebedana21/sql_testing_framework/tree/master/test_project).

You can find how to run the test harness on the project in [the project readme](https://gitlab.com/lebedana21/sql_testing_framework).


Architecture drawbacks 
------------------

There are two drawbacks I am aware of.


First of all, as it was mentioned, when your database stays always clean, a developer does not see what was happening during the test execution.
To overcome the drawback you could add a *teardown* function to your setup script, which will call `COPY` on one or multiple tables inside. The function will be then called after each test execution. 

Also, a test rollback happens only when there is no SQL error during the pg_prove execution. 
Otherwise, the database content will stay modified. This is another reason why go for a database container.  

Feedback
---------

Feel free to apply the test harness in case you find it useful. Feedback, questions and pull requests are very welcome. 

