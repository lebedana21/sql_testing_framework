#!/bin/bash
DATABASE=
HOST=
PORT=5432
USER="postgres"
PASSWORD=
SCHEMA=
FORMATER=""
VERBOSE=""

function usage() { echo "Usage: $0 -h host -d database -p port -s schema_name -x files_to_execute" 1>&2; exit 1; }

while getopts d:h:p:n:s:x:v OPTION
do
  case $OPTION in
    d)
      DATABASE=$OPTARG
      ;;
    h)
      HOST=$OPTARG
      ;;
    p)
      PORT=$OPTARG
      ;;
	s)
      SCHEMA=$OPTARG
      ;; 
	x)
      EXECUTE=$OPTARG
      ;; 
	v)
      VERBOSE="--verbose"
      ;;   
    *)
      usage
      ;;
  esac
done

if [[ -z $DATABASE ]] || [[ -z $HOST ]] || [[ -z $PORT ]] || [[ -z $USER ]] || [[ -z $SCHEMA ]]
then
  usage
  exit 1
fi

psql -h $HOST -p $PORT -d $DATABASE -U $USER -f /pgtap/sql/pgtap.sql > /dev/null
rc=$? # exit if pgtap failed to install
if [[ $rc != 0 ]] ; then
  echo "pgTap was not installed properly. Unable to run tests!"
  exit $rc
fi

cd /tmp/

if [[ -n $EXECUTE ]] 
then 
	for FNAME in $EXECUTE; do 
		psql -h $HOST -p $PORT -d $DATABASE -U $USER -f $FNAME > /dev/null
	done
fi 

pg_prove -h $HOST -p $PORT -d $DATABASE -U $USER $VERBOSE --normalize --schema $SCHEMA --runtests # --verbose
 
psql -h $HOST -p $PORT -d $DATABASE -U $USER -f /pgtap/sql/uninstall_pgtap.sql > /dev/null
# exit with return code of the tests

exit 0



