#!/bin/bash

##################
# initialization #
##################

### links ###

PSQL_IMAGE="postgres:9.6.0"
PGTAP_IMAGE="my-pgtap:latest"
DOCKER_V_FOLDER="tmp"

### db parameters ###

DB_NAME='test_db'

### log ###

DATENOW=$(date +%Y%m%d_%H%M%S)
LOGDIR="log/"
mkdir -p $LOGDIR > /dev/null

### default args values & other  vars ###

KILL_DB=false
VERBOSE_STR=""

### functions ###

function remove_container() {
	CONTAINER_NAME=$1
	echo -e "STEP) Remove container ${CONTAINER_NAME}..\n"
	docker stop $CONTAINER_NAME > /dev/null 2>&1 || true && docker rm $CONTAINER_NAME > /dev/null 2>&1 || true;
}

function run_tests_for_schema() {
	THE_SCHEMANAME=$1
	THE_LOGNAME="${LOGDIR}${THE_SCHEMANAME}_${DATENOW}.log"
	
	echo -e "\nSTEP) Run test suit contained in schema \"${THE_SCHEMANAME}\".."
	docker run -v /$(pwd)/:/tmp/ --rm --network=host $PGTAP_IMAGE -h localhost -p $DB_PORT -d $DB_NAME -s $THE_SCHEMANAME -x "$SFILES $FFILES" $VERBOSE_STR > $THE_LOGNAME && cat $THE_LOGNAME	
}

function usage() { echo "Usage: $0 -n container_name -p db_container_port -t dirname -k" 1>&2; exit 1; }

### read arguments ###

while getopts n:t:p:kv OPTION
do
  case $OPTION in
	n)
      DB_CONT_NAME=$OPTARG
      ;;
	p)
      DB_PORT=$OPTARG
      ;;  
	t)
	  FDIR=$OPTARG
	  ;; 
	k)
      KILL_DB=true
	  ;; 
	v)
      VERBOSE_STR="-v"
	  ;;   
    *)
      usage
      ;;
  esac
done

if [[ -z $FDIR ]] || [[ -z $DB_CONT_NAME ]] || [[ -z $DB_PORT ]]
then
  usage
  exit 1
fi

######################
# get latest images  #
######################

docker pull $PSQL_IMAGE > /dev/null

####################################################
# parse given dir on test and setup(startup) files #
####################################################

FFILES=$(find $FDIR -name 'test_*.sql')

# check a file is given 
if [[ ${FDIR##*.} == "sql" ]]; then
	FDIR="$(dirname $FDIR)/"
	echo "NOTE) A test file is given. Search startup file(s) in dir \"$FDIR\""
fi	

SFILES=$(find $FDIR -name 'startup*.sql')
echo $SFILES

################################
# create container with the DB #
################################

TESTDIR="/$(pwd)/$LOGDIR" # "/$(pwd)/${FDIR}" 

CONT_COUNT=$(docker ps -a | grep "$DB_CONT_NAME" | wc -l)
if (($CONT_COUNT < 1)); then
	{
		{
			echo -e "\nSTEP) Container '$DB_CONT_NAME' doesn't exist. Create" 
			docker run -d -p $DB_PORT:5432 --name $DB_CONT_NAME $PSQL_IMAGE; sleep 4s #  -v $TESTDIR:/$DOCKER_V_FOLDER 
			docker exec $DB_CONT_NAME bash -c "chmod 775 /tmp"
		} && {		
			echo -e "\nSTEP) Initiate DB.." 
			docker exec $DB_CONT_NAME psql -U postgres -c "CREATE DATABASE $DB_NAME;" #> /dev/null 2>&1
		} && echo -e "DONE\n"
		
	} || { 
		echo -e "\nERROR) Something went wrong during DB initiation. Remove $DB_CONT_NAME and exit.\n"
		remove_container $DB_CONT_NAME
		exit 1
	}
else
	echo -e "NOTE) Container '$DB_CONT_NAME' exists\n"
	docker start $DB_CONT_NAME  > /dev/null # just in case cont is down
fi


###################################
# run tests for each found schema #
###################################

SCHEMES=$(cat $SFILES $FFILES | grep -iF "create schema" | grep -o "[^ ]*;" | sed 's/\;//g') # get name(s) of the schemes
echo -e "NOTE) FOUND SCHEMES ARE: \n\"$SCHEMES\""
for THE_SCHEMA in $SCHEMES; do 
	run_tests_for_schema $THE_SCHEMA
done 

echo -e "-------DONE--------\n------------------"

if [ $KILL_DB = true ] ; then
	remove_container $DB_CONT_NAME
fi
