Receipt parser test project
---------------------------


The project implements a primitive receipt parser. The main function [doc_parsing.parse_receipts()](https://gitlab.com/lebedana21/sql_testing_framework/blob/master/test_project/startup_functions.sql#L41) is aimed at structuring documents of type 'receipt' contained in table `doc_parsing.docs`.
It extracts receipt id with [doc_parsing.get_receipt_id()](https://gitlab.com/lebedana21/sql_testing_framework/blob/master/test_project/startup_functions.sql#L20)
and its item ids with [doc_parsing.get_items_id()](https://gitlab.com/lebedana21/sql_testing_framework/blob/master/test_project/startup_functions.sql#L1) and returns a table containing document id, receipt id and item id for each extracted item.
  
For the project, a valid receipt contains exactly one id and at least one item. 
Here is an example of a valid receipt with two items:

```
Receipt identifier: rec1 

Item identifier: item1 
Item identifier: item2
```

Project content
-------------------
 
 
* [startup.sql](startup.sql) contains functionality essential for [the test harness](https://gitlab.com/lebedana21/sql_testing_framework) to run properly and common functions specific for the test suite. Those are

   * `doc_parsing.startup_initialize_docs_table()` initializing `doc_parsing.docs` table
   * `doc_parsing.fill_db()` inserting values into `doc_parsing.docs` table
   * `doc_parsing.create_expected_tb()` creating and filling `doc_parsing.tmp_results` table

* [startup_functions.sql](startup_functions.sql) contains functions to test.
In practice, those functions might already exist in the database.

* [test_get_receipt_id.sql](test_get_receipt_id.sql), [test_get_items_id.sql](test_get_items_id.sql), [test_parse_receipts.sql](test_parse_receipts.sql), each represents a test case, containing multiple tests.


Tests
--------

* [test_get_receipt_id.sql](test_get_receipt_id.sql) contains several unit tests for the `doc_parsing.get_receipt_id()` function
* [test_get_items_id.sql](test_get_items_id.sql) contains several unit tests for the `doc_parsing.get_items_id()` function
* [test_parse_receipts.sql](test_parse_receipts.sql) contains several integration tests for the `doc_parsing.parse_receipts()` function


