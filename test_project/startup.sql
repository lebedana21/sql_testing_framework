-- drop and create the schema
-- drop schema if exists doc_parsing cascade;
create schema if not exists doc_parsing;

create or replace function doc_parsing.startup_initialize(
) returns void as
$func$
begin
  set client_min_messages to warning; -- to suppress warnings in log
end
$func$ language plpgsql;


-- create doc_parsing.docs table
-- Note: in general, the table with no content will be created during test DB initialization
create or replace function doc_parsing.startup_initialize_docs_table()
returns void as
$func$
begin
  drop type if exists doc_types cascade;
  create type doc_types as ENUM ('receipt', 'other');

  drop table if exists doc_parsing.docs;
  create table doc_parsing.docs
  (
    doc_id bigint not null primary key,
    doc_content text,
    doc_type doc_types not null
  );

end
$func$ language plpgsql;

create or replace function doc_parsing.fill_db(doc_id_arr bigint[]
                                               , doc_content_arr text[]
                                               , doc_type_arr text[]
) returns void as
$func$
begin

  -- fill doc_parsing.docs table
  for i in 1..array_length(doc_id_arr, 1)
    loop
      insert into doc_parsing.docs
      values (doc_id_arr[i], doc_content_arr[i], doc_type_arr[i]::doc_types);
  end loop;
end
$func$ language plpgsql;


create or replace function doc_parsing.create_expected_tb(doc_id_arr bigint[]
                                                          , receipt_id_arr text[]
                                                          , item_id_arr text[]
) returns void as
$func$
begin

  -- create expected output table
  create table doc_parsing.tmp_results
  (
    doc_id     bigint,
    receipt_id text,
    item_id    text

  );

  -- fill expected output table
  for i in 1..array_length(doc_id_arr, 1)
    loop
      insert into doc_parsing.tmp_results
      values (doc_id_arr[i], receipt_id_arr[i], item_id_arr[i]);
    end loop;

end;
$func$ language plpgsql;


-- finally, functions starting with 'shutdown' will be executed after all tests run
create or replace function doc_parsing.shutdown_drop_schema(
) returns VOID as
$func$
begin
  drop schema if exists doc_parsing cascade;
end
$func$ language plpgsql;