create or replace function doc_parsing.get_items_id(receipt_content text) returns text[]
  language plpgsql
as
$$
declare
  item_idregix text:= '\n\nItem identifier\:[ ]*([^\n ]+)';
  item_id_extracted text[];
begin

  item_id_extracted:= array(select array_to_string(regexp_matches(receipt_content, item_idregix, 'g'), ''));
  -- invalid receipts
  if coalesce(array_length(item_id_extracted, 1), 0) < 1 then
    raise exception 'Receipt contains no item';
  end if;

  return item_id_extracted;
end;
$$;

create or replace function doc_parsing.get_receipt_id(receipt_content text) returns text
  language plpgsql
as
$$
declare
  receipt_idregix text:= 'Receipt identifier\:[ ]*([^\n ]+)';
  receipt_id_extracted text[];
begin

  receipt_id_extracted := array(select array_to_string(regexp_matches(receipt_content, receipt_idregix, 'g'), ''));
  -- invalid receipts
  if coalesce(array_length(receipt_id_extracted, 1), 0) < 1 then
    raise exception 'Receipt has no id';
  elseif array_length(receipt_id_extracted, 1) > 1 then
    raise exception 'Receipt has multiple ids';
  end if;

  return receipt_id_extracted[1];
end;
$$;

create or replace function doc_parsing.parse_receipts() returns table(doc_id bigint
                                                                    , receipt_id text
                                                                    , item_id text)
  language plpgsql
as
$$
begin

return query

with receipts_parsed as (
  select d.doc_id
      , doc_parsing.get_receipt_id(d.doc_content)       as receipt_id
      , unnest(doc_parsing.get_items_id(d.doc_content)) as item_id
  from doc_parsing.docs as d
  where d.doc_type = 'receipt'::doc_types
)

select * from receipts_parsed rp
where rp.receipt_id is not null;

end;
$$;
