CREATE OR REPLACE FUNCTION doc_parsing.test_get_items_id_returns_id(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'Receipt identifier: 1 \n\nItem identifier: item1';
  return next results_eq(format('select unnest(doc_parsing.get_items_id(''%s''::text))', doc_content)
                        , array['item1']);

  end
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION doc_parsing.test_get_items_id_returns_id_without_spaces(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'Receipt identifier: 1 \n\nItem identifier: item1 amount=5';
  return next results_eq(format('select unnest(doc_parsing.get_items_id(''%s''::text))', doc_content)
                        , array['item1']
                        , 'returns id without spaces when line continues');
  end
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION doc_parsing.test_get_items_id_returns_id_when_two_items(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'Receipt identifier: 1 \n\nItem identifier: item1\n\nItem identifier: item2';
  return next results_eq(format('select unnest(doc_parsing.get_items_id(''%s''::text))', doc_content)
                        , array['item1', 'item2']);
  end
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION doc_parsing.test_get_items_id_returns_id_when_more_newlines(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'Receipt identifier: 1 \n\n\n\nItem identifier: item1';
  return next results_eq(format('select unnest(doc_parsing.get_items_id(''%s''::text))', doc_content)
                        , array['item1']);

  end
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION doc_parsing.test_get_items_id_raise_when_no_items(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'Receipt identifier: 1';
  return next throws_ok(format('select unnest(doc_parsing.get_items_id(''%s''::text))', doc_content)
                        , 'Receipt contains no item');
  end
$$ LANGUAGE plpgsql;
