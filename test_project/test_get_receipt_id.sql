CREATE OR REPLACE FUNCTION doc_parsing.test_get_receipt_id_returns_id(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'Receipt identifier: 1 \n\nItem identifier: item1';
  return next results_eq(format('select doc_parsing.get_receipt_id(''%s''::text)', doc_content)
                        , array['1']);

  end
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION doc_parsing.test_get_receipt_id_returns_id_when_header_present(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'THIS THIS A HEADER \nReceipt identifier: 1';
  return next results_eq(format('select doc_parsing.get_receipt_id(''%s''::text)', doc_content)
                        , array['1']);
  end
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION doc_parsing.test_get_receipt_id_raise_when_no_id(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'\n\nItem identifier: item1';
  return next throws_ok(format('select doc_parsing.get_receipt_id(''%s''::text)', doc_content)
                        , 'Receipt has no id');
  end
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION doc_parsing.test_get_receipt_id_raise_when_multiple_ids(
) RETURNS SETOF TEXT AS $$

  declare
    doc_content text;
  begin

  doc_content:= E'Receipt identifier: 1 \nReceipt identifier: 2';
  return next throws_ok(format('select doc_parsing.get_receipt_id(''%s''::text)', doc_content)
                        , 'Receipt has multiple ids');
  end
$$ LANGUAGE plpgsql;
