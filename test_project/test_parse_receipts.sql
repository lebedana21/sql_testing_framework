create or replace function doc_parsing.test_parse_receipts_one_doc(
) returns setof text as
$$
begin

  perform doc_parsing.fill_db(array[1]
                                    , array[E'Receipt identifier: id-1 \n\nItem identifier: item1']
                                    , array['receipt']);

  perform doc_parsing.create_expected_tb(array[1]
                                        , array['id-1']
                                        , array['item1']);

  return next results_eq('select * from doc_parsing.parse_receipts()'
                         , 'select * from doc_parsing.tmp_results'
                         , 'parses valid receipt doc when it contains one item');
end
$$ language plpgsql;

create or replace function doc_parsing.test_parse_receipts_multiple_items(
) returns setof text as
$$
begin

  perform doc_parsing.fill_db(array[1]
                                    , array[E'Receipt identifier: id-1 \n\nItem identifier: item1 \n\nItem identifier: item2']
                                    , array['receipt']);

  perform doc_parsing.create_expected_tb(array[1, 1]
                                        , array['id-1', 'id-1']
                                        , array['item1', 'item2']);

  return next results_eq('select * from doc_parsing.parse_receipts()'
                         , 'select * from doc_parsing.tmp_results'
                         , 'parses valid receipt doc when it contains two items');
end
$$ language plpgsql;

create or replace function doc_parsing.test_parse_receipts_raise_when_multiple_rec_id(
) returns setof text as
$$
begin

  perform doc_parsing.fill_db(array [1]
                              , array [E'Receipt identifier: id-1 \n\nItem identifier: item1 \n Receipt identifier: id-2']
                              , array ['receipt']);

  return next throws_ok('select * from doc_parsing.parse_receipts()');
end
$$ language plpgsql;


create or replace function doc_parsing.test_parse_receipts_raise_when_no_items(
) returns setof text as
$$
begin

  perform doc_parsing.fill_db(array[1]
                              , array [E'Receipt identifier: id-1']
                              , array ['receipt']);

  return next throws_ok('select * from doc_parsing.parse_receipts()');
end
$$ language plpgsql;

create or replace function doc_parsing.test_parse_receipts_raise_when_empty_doc(
) returns setof text as
$$
begin

  perform doc_parsing.fill_db(array[1]
                              , array ['']
                              , array ['receipt']);

  return next throws_ok('select * from doc_parsing.parse_receipts()');
end
$$ language plpgsql;


create or replace function doc_parsing.test_parse_receipts_raise_when_null_given(
) returns setof text as
$$
begin
  perform doc_parsing.fill_db(array[1]
                              , array ['']
                              , array ['receipt']);

  return next throws_ok('select * from doc_parsing.parse_receipts()');
end
$$ language plpgsql;